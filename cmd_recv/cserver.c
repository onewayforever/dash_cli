#include <czmq.h>
#include <signal.h>

int loop_flag = 1;
void stop_loop(int sig)
{
    loop_flag = 0;
}

int main (void)
{
    sigset_t set;
    //    zsock_t *push = zsock_new_push ("inproc://example");
    //    zsock_t *pull = zsock_new_pull ("inproc://example");
    zsock_t *rep = zsock_new_rep("ipc:///tmp/config_channel.ipc");
    //    zstr_send (push, "Hello, World");
    char *str=NULL;
    sigemptyset(&set);
    sigprocmask(SIG_SETMASK, &set, NULL);
    signal(SIGINT, stop_loop);
    while (loop_flag){
            str=zstr_recv(rep);
            if(str){
                    printf("Get:%s\n",str);
                    zstr_send (rep, "ack");
                    zstr_free(&str);
            }
            else
                    usleep(10);
    }
    zsock_destroy (&rep);

        /*
           char *string = zstr_recv (pull);
           puts (string);
           zstr_free (&string);

           zsock_destroy (&pull);
           zsock_destroy (&push);
           */
        return 0;
}
