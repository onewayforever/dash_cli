# to add process function for cli cmd here 
#function: 
#     input: <view_name>  <arg_of_view> <args dict> 
#     return: <view_name> <arg_of_view>
#     do not change view_name and arg_of_view if not to change the view
import zmq
import getpass
context = None
socket_dict = {}

#call init_cli_func in mycli
# add init process here
CHANNEL_RULE = 'rule_channel'
CHANNEL_CONFIG = 'config_channel'

def init_cli_func():
    global context
    global socket_dict
    print 'Init cli func ...'
    context = zmq.Context()
    
    socket_dict[CHANNEL_RULE] =  context.socket(zmq.REQ)
    socket_dict[CHANNEL_CONFIG] =  context.socket(zmq.REQ)

    #socket_dict[CHANNEL_RULE].connect("ipc://"+CHANNEL_RULE+".ipc")
    print "ipc:///tmp/"+CHANNEL_CONFIG+".ipc"
    socket_dict[CHANNEL_CONFIG].connect("ipc:///tmp/"+CHANNEL_CONFIG+".ipc")

    return

def default_prompt_to_string(user,view,view_arg):
    if view_arg:
        return view+' '+str(view_arg)+'>>'
    else:
        return view+'>>'

def view_switch(user,view,view_arg,fun_arg,args):
    #check if view change
    if args == {"exit":True} or args == {"logout":True}:
        return None,{}
    #neglect check arg, add later
    new_view = None
    new_view_arg = {}
    for key in args:
        if isinstance(args[key],bool) and args[key]:
            new_view = key
        else:
            new_view_arg[key] = args[key]
    #neglect check arg, add later
    #print args
    return new_view,new_view_arg

def issue_cmd(user,view,view_arg,fun_arg,args):
    #global socket_dict
    #print socket_dict[CHANNEL_CONFIG]
    global socket_dict
    socket_dict[CHANNEL_CONFIG].send_json(args)
    get = socket_dict[CHANNEL_CONFIG].recv()
    #get = 'ack'
    print 'issue dict:'+str(args)+' get:'+get
    return view,view_arg

    



