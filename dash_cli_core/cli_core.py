import re
import getpass  
import hashlib


cli_match_tree={}
func_map ={}

conf_file=None

'''
    arg{key,checker}
'''

pattern_re_IP = None
pattern_re_INT = None
pattern_re_HEX = None
user_dict = {}
user_file = None

class cli_arg:
    def __init__(self,key,type,checker,term_flag,process_fun,process_fun_arg):
        self.key = key
        self.type = type
        self.checker = checker
        self.term_flag = term_flag
        self.process_fun = process_fun
        self.process_fun_arg = process_fun_arg
        self.hint = []
        self.next = []

def init_cli_user(filename):
    global user_dict
    global user_file
    file = open(filename,"r")
    if not file:
        print 'Error: open user file failed, please check:'+filename
        exit(-1)
    for line in file:
        line = line.strip() 
        if len(line)==0:
            continue
        if line.find('$')>=0: #comment by $
            continue
        parse = line.split(':')
        if len(parse) < 2:
            print 'Error: parse user file failed,please check:'+filename 
            exit(-1)
        user_dict[parse[0]]={}
        user_dict[parse[0]]['uid']=int(parse[1])
        if len(parse) == 3:
            user_dict[parse[0]]['passwd'] = parse[2]
        else:
            user_dict[parse[0]]['passwd'] = None
    user_file = filename 
    file.close()
    while True:
        user = raw_input("login:") 
        if not user_dict.get(user):
            print 'Invalid user'
            continue
        if not user_dict[user]['passwd']:
            return {'user':user,'uid':user_dict[user]['uid']}

        for i in range(3):
            passwd = getpass.getpass("passwd:") 
            passwd_md5 = hashlib.md5(passwd).hexdigest()
            if passwd_md5 ==user_dict[user]['passwd']:
                return {'user':user,'uid':user_dict[user]['uid']}
            
        

def add_to_match_tree(current_fork,arg,hint,term_flag,process_fun,process_fun_arg):
    key,type,checker = arg
    for pos in current_fork:
        if pos.key == key and pos.type == type:
            if pos.term_flag == False:
                pos.term_flag = term_flag
            pos.hint.append(hint)
            return pos.next
    new_arg = cli_arg(key,type,checker,term_flag,process_fun,process_fun_arg)
    new_arg.hint.append(hint)
    current_fork.append(new_arg)
    return new_arg.next

def mk_checker(type):
    if type=='INT': #case Int
        #print 'find Int'
        def checker(value):
            match = pattern_re_INT.match(value)
            if match:
                return True,int(value)
            else:
                return False,None
        return checker
    elif type=='PORT': #case Int
        #print 'find Port'
        def checker(value):
            print value
            match = pattern_re_INT.match(value)
            if match and int(value)<=65535:
                return True,int(value)
            else:
                return False,None
        return checker    
    elif type=='0|1': #0 or 1
        def checker(value):
            if value == '0' or value =='1':
                return True,int(value)
            else:
                return False,None
        return checker    
    elif type=='IP': #IP
        #print 'find IP'        
        def checker(value):
            if value == '0':
                return True,'0.0.0.0'
            match = pattern_re_IP.match(value)
            if match:
                return True,value
            else:
                return False,None
        return checker
    elif type=='INTERFACE': # 
        #print 'find Interface'
        def checker(value): ### just int now ,may change to string later#####
            match = pattern_re_INT.match(value)
            if match:
                return True,value
            else:
                return False,None
        return checker    
    elif type=='STR': # 
        #print 'find Interface'
        def checker(value):
            return True,value
        return checker    
    elif type=='HEX': #hex
        def checker(value):
            if value == '0':
                return True,0
            match = pattern_re_HEX.match(value)
            if match:
                return True,int(value,16)
            else:
                return False,None
        return checker    
    elif type=='USER': #hex
        def checker(value):
            global user_dict
            if user_dict.get(value):
                return True,value
            else:
                return False,None
        return checker    
    elif len(type.split("|")) > 1: #case option
        options = type.split("|")
        #print 'find options'
        #print options
        def checker(value):
            if value in options:
                return True,value
            else:
                return False,None
        return checker
    
        

def parse_arg(arg):
    #match (option1|option2) <input>
    #return arg,'const',None
    p_value = re.compile("<.+#.+>")
    p_label = re.compile("[A-Za-z0-9_-]+")
    match = p_value.match(arg) #is set key=value?
    if match:
        #print 'real checker'
        parse = arg.strip("<>").split("#")
        #print parse[0]
        #print parse[1]
        return parse[0],'value',mk_checker(parse[1])
    elif p_label.match(arg): #is set label=true?
        def checker(value):
            if value==arg:
                return True,True
            else:
                return False,None
        return arg,'label',checker
    else:
        print arg
        print 'maybe error in '+conf_file+', please check'
        exit(-1)
        #print 'fake'
        #return arg,'const',None


def parse_view(line):
    p = re.compile("^VIEW\:.+")
    match = p.match(line)
    if match:
        return match.group().split(":")[1]
    else: 
        return None

def parse_input_format(view,line):
    global cli_match_tree
    if not view:
        return None
    cmd_process_fun_arg = None
    if len(line.split('@')) > 1:
        if len((line.split('@')[1]).split('#')) > 1:
            cmd_process_fun_arg = (line.split('@')[1]).split('#')[1]
        else:
            cmd_process_fun_arg = None
        cmd_process_fun = func_map.get((line.split('@')[1]).split('#')[0]) 
    
        if not cmd_process_fun:
            print line
            print 'Error: Get func:'+line.split('@')[1]+' failed! check '+conf_file
            exit(-1)
        input=line.split('@')[0] .split()
    else:
        input=line.split()
        cmd_process_fun = default_process_fun
    process_fun = None
    process_fun_arg = None
    current_fork = cli_match_tree[view]
    term_flag=False
    for arg in input:
        #print arg
        (key,type,checker) = parse_arg(arg)
        if input.index(arg)==len(input)-1:
            term_flag=True
            process_fun = cmd_process_fun
            process_fun_arg = cmd_process_fun_arg
        current_fork = add_to_match_tree(current_fork,(key,type,checker)," ".join(input),term_flag,process_fun,process_fun_arg)

    return input

def build_match_tree(filename,my_func_map):
    global func_map
    global pattern_re_IP
    global pattern_re_INT
    global pattern_re_HEX

    pattern_re_IP = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$") 
    pattern_re_INT = re.compile("^\d+$")
    pattern_re_HEX = re.compile("^0x[0-9a-fA-F]+$")

    func_map = my_func_map
    file = open(filename,"r")
    if not file:
        print 'Error: open conf file failed, please check:'+filename
        exit(-1)
    conf_file=filename
    for line in file:
        line = line.strip() 
        if len(line)==0:
            continue
        if line.find('$')>=0: #comment by $
            continue
        view = parse_view(line)
        if view:
            current_view = view
            cli_match_tree[view]=[]
            continue
        parse_input_format(current_view,line)
    file.close()

def print_arg_tree(arg_tree):
    #print arg_tree.key
    for arg in arg_tree.next:
        print_arg_tree(arg)
    
def print_match_tree():
    global cli_match_tree
    for view in cli_match_tree:
        #print '-----' + view + '------'
        for arg_tree in cli_match_tree[view]:
            print_arg_tree(arg_tree)

def match_arg(current_fork,arg,acc):
    #key = check_arg(arg)
    #type = 'const'
    hint_list = []
    for pos in current_fork:
        if pos.checker:
            #print 'use checker'
            flag,value = pos.checker(arg)
            if flag:
            #if pos.key == key and pos.type == type:
                key=pos.key
                acc[key]=value
                hint_list=[]
                for x in pos.hint:
                    hint_list.append(x)
                return True,pos.next,acc,hint_list,pos.term_flag,pos.process_fun,pos.process_fun_arg
            else:
                for x in pos.hint:
                    hint_list.append(x)
        else:
            print 'why?'
            #if pos.key == arg and pos.type == type:
            #    key=pos.key
            #    acc[key]=True
            #    return True,pos.next,acc    
    return False,current_fork,acc,hint_list,False,None,None #(if match) (pos in tree) (value dict) (hint list) (can be finish?) (process func) (process fun arg)

def match_input(view,line):
    global cli_match_tree
    if not view:
        return None
    line = line.strip() 
    if len(line)==0:
        return None
    input=line.split()
    current_fork = cli_match_tree[view]
    acc = {}
    error_pos = 0
    hint_list = []
    flag_find_false=False
    term_flag=False
    process_fun = None
    process_fun_arg = None
    for arg in input:
        if current_fork==[]:
            flag_find_false=True
            break
        flag,current_fork,acc,hint_list,term_flag,process_fun,process_fun_arg = match_arg(current_fork,arg,acc)
        if not flag:
            return False,error_pos,'error',acc,hint_list,None,None #input error
        else:
            error_pos = error_pos + 1
    if not flag_find_false and current_fork==[]:
        return True,0,'ok',acc,hint_list,process_fun,process_fun_arg
    elif not flag_find_false and term_flag == True :
        return True,0,'ok',acc,hint_list,process_fun,process_fun_arg
    else:
        return False,error_pos,'more',acc,hint_list,None,None #input incompleted 
        
            
def test_match_tree():
    print (match_input("system","testint 93"))
    print (match_input("system","testint 93 43"))
    print (match_input("system","testint 93 43 241"))
    print (match_input("system","testoption 32 on"))
    print (match_input("system","testoption 32 off"))
    print (match_input("system","testoption 32 other"))
    print (match_input("system","testoption fake other"))
    print (match_input("system","testoption 14"))
    print (match_input("system","show interface"))
    print (match_input("system","show acl"))
    print (match_input("acl","flex 127.0.0.1 23.122.23.2 6300 80"))
    print (match_input("acl","no flex 127.0.0.1 23.122.23.2 6300 80"))
    print (match_input("acl","no flex 127.0.0.1 23.122.23.2 6300 80 23"))
    print (match_input("acl","no flex 127.0.0.1 22 6300 80 23"))
    print (match_input("acl","no flex 127.0.0.1 23.122.23.2 6300"))
    return

def default_process_fun(user,view,view_arg,func_arg,args):
    print args
    return view,view_arg
    
def default_prompt_to_string(user,view,view_arg):
    if view_arg:
        return view+' '+view_arg+'>>'
    else:
        return view+'>>'

def check_view(view):
    if cli_match_tree.get(view):
        return True
    else:
        return False

def mypasswd(user,view,view_arg,fun_arg,args):
    to_user = user['user']
    if args.get('user'):
        if user['user']!=args.get('user') and user['user']!='admin':
            print 'Change Password Forbid!' 
            return view,view_arg
        if not user_dict.get(args.get('user')):
            print 'Can not find user!'
            return view,view_arg
        to_user =  args.get('user')

    if user_dict[user['user']]['passwd']:
        password = getpass.getpass('Enter password: ') 
        password_md5 = hashlib.md5(password).hexdigest()
        if password_md5 !=  user_dict[user['user']]['passwd']:
            print 'Password mismatch!'
            return view,view_arg

    password1 = getpass.getpass('Enter New password: ')
    password2 = getpass.getpass('Confirm password: ')

    if password1 != password2:
        print 'Pasaword missmatch!'    
        return view,view_arg
    
    #print to_user
    user_dict[to_user]['passwd'] =  hashlib.md5(password1).hexdigest()

    file = open(user_file,'w')
    for key in user_dict:
        line = '%s:%d'%(key,user_dict[key]['uid'])    
        if user_dict[key]['passwd']:
            line=line+':%s'%user_dict[key]['passwd']
        file.write(line+'\n')
    file.close()
    return view,view_arg
        
