#! /usr/bin/python
import os
import sys
import dash_cli_core.func_core as fnc
import dash_cli_core.cli_core as clc 
#from dash_x_cli_func import init_cli_func
from dash_s_cli_func import prompt_to_string 
from dash_s_cli_func import func_map 

import signal
import readline

user_file = "user.conf"
conf_file = "cli.conf"




def sighandler(sig,id):
    if sig==signal.SIGINT or sig == signal.SIGTERM:
        exit(-1)


if __name__ == '__main__':

    readline.parse_and_bind('tab: complete') 
    #readline.parse_and_bind('tab: accept-line') 

    readline.set_completer(lambda (text,state): None )

    signal.signal(signal.SIGTERM,sighandler)
    signal.signal(signal.SIGINT,sighandler)

    
    fnc.init_cli_func()

    clc.build_match_tree(conf_file,func_map)
    
    
    #print_match_tree()
    #test_match_tree()
    while True:
        current_user = clc.init_cli_user(user_file)
        (current_view,view_arg) = ("system",{})
        process_fun = None
        process_fun_arg = None
        while True:
            my_input=raw_input(prompt_to_string(current_user,current_view,view_arg))
            #my_input=my_raw_input(prompt_to_string(current_view,view_arg))
            #print my_input
            if len(my_input.strip())==0:
                continue
            result,pos,info,to_issue,hint_list,process_fun,process_fun_arg = clc.match_input(current_view,my_input)
            #print (result,info)
            if result:
                if not process_fun:
                    print "BUG, correct input without processing function, check cli.conf"
                    exit(-1)
                current_view,view_arg = process_fun(current_user,current_view,view_arg,process_fun_arg,to_issue)
                if not current_view:
                    break #call exit/logout
                elif not clc.check_view(current_view):
                    print 'Error: change to a unset view [' + current_view + ']! please check ' + conf_file
            else:
                print 'Input Error: ' + " ".join(my_input.strip().split())
                print ' '*(len('Input Error: ')+len(" ".join(my_input.strip().split()[0:pos]))+1)+'^'
                print 'Hint:'
                for s in hint_list:
                    print s
