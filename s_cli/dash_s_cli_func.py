# to add process function for cli cmd here 
#function: 
#     input: <view_name>  <arg_of_view> <args dict> 
#     return: <view_name> <arg_of_view>
#     do not change view_name and arg_of_view if not to change the view
import dash_cli_core.func_core as fnc
import dash_cli_core.cli_core as clc 

def prompt_to_string(user,view,view_arg):
    #print view_arg
    if view_arg != {}:
        if view == 'interface':
            return '<%s@%s %s %s>'%(user['user'],'flowfirm',view,view_arg['interface-no'])
        elif view == 'group':
            return '<%s@%s %s (%s:%d)>'%(user['user'],'flowfirm',view,view_arg['username'],view_arg['group-no'])

    else:
        return '<%s@%s %s>'%(user['user'],'flowfirm',view)
    

def check_rule_mode(sip,dip,sport,dport,proto):
    msip = 1
    mdip = 1
    msport = sport 
    mdport = dport
    mproto = proto
    if sip=='0.0.0.0':
        msip=0
    if dip=='0.0.0.0':
        mdip=0
    tuple_msk = '0b%d%d%d%d%d'%(mproto,mdport,msport,mdip,msip)
    return int(tuple_msk,2)


def issue_ipv4(user,view,view_arg,fun_arg,args):
    my_args = args
    my_args['cmd_name'] = fun_arg
    my_args['user_id'] = user['uid'] 
    my_args['ip_type'] = "ipv4"
    my_args['rule_mode'] = check_rule_mode(args['msip'],args['mdip'],args['msport'],args['mdport'],args['mproto'])
    my_args['port_group'] = args['if-group-no']
    my_args['hit-num'] = args['hit-stat']
    
    return fnc.issue_cmd(user,view,view_arg,fun_arg,my_args)

def issue_port_group(user,view,view_arg,fun_arg,args):
    my_args = args
    my_args['cmd_name'] = fun_arg
    print user
    print view
    print view_arg
    my_args['user_id'] = clc.user_dict[view_arg['username']]['uid']
    my_args['port_group_id'] = view_arg['group-no']
    if args.get('port-name'):
        my_args['port'] = int(args['port-name'])
    lba = args.get('lba')
    if lba:
        lba_mask = 0
        if lba == 'sip':
            lba_mask = 0b00001
        elif lba == 'dip':
            lba_mask = 0b00010
        elif lba =='sip-dip':
            lba_mask = 0b00011
        elif lba =='sip-dip-sport-dport':
            lba_mask = 0b01111
        elif lba == 'sip-dip-sport-dport-protocol':
            lba_mask = 0b11111
        my_args['lba'] = lba_mask 
    return fnc.issue_cmd(user,view,view_arg,fun_arg,my_args)

def issue_user(user,view,view_arg,fun_arg,args):
    my_args = args
    my_args['cmd_name'] = fun_arg
    my_args['user_id'] = user['uid']
    if fun_arg == 'set-user-action':
        if args.get('no-forward'):
            my_args['action'] = 'drop'
        elif args.get('forward'):
            my_args['action'] = 'forward'
            my_args['port_group_id'] = args['if-group-no']
    return fnc.issue_cmd(user,view,view_arg,fun_arg,my_args)


    
func_map ={
    "view_switch":fnc.view_switch,
    "issue_cmd":fnc.issue_cmd,
    "issue_ipv4":issue_ipv4,
    "issue_port_group":issue_port_group,
    "issue_user":issue_user,
    "passwd":clc.mypasswd
}



